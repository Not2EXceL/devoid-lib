package io.not2excel.devoid.syringe

import java.io.PrintWriter

import org.objectweb.asm.{ClassReader, ClassWriter}
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.util.CheckClassAdapter

trait Transformer {

    final def transform(name: String, bytes: Array[Byte]): Array[Byte] = {
        val classReader = new ClassReader(bytes)
        val classNode = new ClassNode()
        classReader.accept(classNode, 0)
        inject(classReader, classNode)
        val classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS)
        classNode.accept(classWriter)
        val finalBytes = classWriter.toByteArray
        CheckClassAdapter.verify(new ClassReader(finalBytes), false, new PrintWriter(System.err))
        finalBytes
    }

    def inject(classReader: ClassReader, classNode: ClassNode)
}
