package io.not2excel.devoid.syringe

import java.io.File
import java.nio.file.Files

trait Injector {

    def inject(transformers: Map[MappedClass, Transformer], classes: List[File]): List[Class[_]] = {
        classes.map { f => Files.readAllBytes(f.toPath) }
        return null
    }
}
