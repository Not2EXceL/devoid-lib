package io.not2excel.devoid.syringe

case class MappedClass(deobfuscated: String, obfuscated: String, description: String) {

}
