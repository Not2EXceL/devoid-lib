package io.not2excel.devoid.lib.util.implicits

import java.io.{InputStream, IOException}

object ClassImplicits {

    implicit class ClassToByteList(val list: List[Class[_]]) {
        def toBytes = list.map(bytesFromClass)
    }

    implicit class ClassToBytes(val c: Class[_]) {
        def toBytes = bytesFromClass(c)
    }

    @throws[IOException]
    def bytesFromClass(c: Class[_]) = {
        bytesFromStream(c.getClassLoader.getResourceAsStream(s"${c.getName.replace(".", "/")}.class"))
    }

    @throws[IOException]
    def bytesFromStream(input: InputStream) = {
        Iterator.continually(input.read).takeWhile(_ != -1).map(_.toByte).toArray
    }
}
