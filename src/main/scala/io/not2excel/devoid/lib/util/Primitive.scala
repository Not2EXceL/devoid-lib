package io.not2excel.devoid.lib.util

sealed abstract class Primitive[T]

object Primitive {

    implicit object BytePrimitive extends Primitive[Byte]

    implicit object CharPrimitive extends Primitive[Char]

    implicit object ShortPrimitive extends Primitive[Short]

    implicit object IntPrimitive extends Primitive[Int]

    implicit object LongPrimitive extends Primitive[Long]

    implicit object FloatPrimitive extends Primitive[Float]

    implicit object DoublePrimitive extends Primitive[Double]

    implicit object BooleanPrimitive extends Primitive[Boolean]

    implicit object StringPrimitive extends Primitive[String]

}
