package io.not2excel.devoid.lib.util.classloading

import java.util.logging.{Level, Logger}

import scala.collection.immutable.HashMap

case class LoadedClasses(classLoader: CustomClassLoader) extends Iterable[Class[_]] {

    private val logger: Logger = Logger.getLogger(classLoader.name)
    private var _classMap: Map[String, Class[_]] = HashMap()

    def classMap: Map[String, Class[_]] = _classMap

    def get(className: String) = _classMap.get(className)

    def addClass(clazz: Class[_]): Unit = {
        if(clazz == null) {
            logger.log(Level.WARNING, "Cannot add null class object.")
        } else {
            if(!_classMap.contains(clazz.getCanonicalName)) _classMap += (clazz.getCanonicalName -> clazz)
            //else logger.log(Level.WARNING, s"${clazz.getName} => Already loaded. Skipping.")
        }
    }

    def addClass(clazz: Option[Class[_]]): Unit = {
        clazz match {
            case Some(c) => addClass(c)
            case _ =>
                logger.log(Level.WARNING, "Cannot add empty class option.")
        }
    }

    def addClasses(classes: Class[_]*) = classes.foreach(addClass)

    def addClasses(classes: Set[Class[_]]) = classes.foreach(addClass)

    def addClasses(classes: List[Class[_]]) = classes.foreach(addClass)

    override def isEmpty: Boolean = _classMap.isEmpty

    override def size: Int = _classMap.size

    override def iterator: Iterator[Class[_]] = _classMap.valuesIterator

    override def toString: String = _classMap map { case (s, c) => s"${c.getSimpleName} => $s\n" } mkString "\n"
}
