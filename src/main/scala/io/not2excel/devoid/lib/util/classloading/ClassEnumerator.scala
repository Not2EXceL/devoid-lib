package io.not2excel.devoid.lib.util.classloading

import java.io.{File, IOException}
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{FileVisitResult, FileVisitor, Files, Path}
import java.util
import java.util.jar.JarFile
import java.util.logging.{Level, Logger}

import scala.collection.immutable.HashMap
import scala.language.postfixOps
import scala.collection.JavaConverters._

object ClassEnumerator {

    private val logger = Logger.getLogger("ClassEnumerator")

    def codeSourcePath = this.getClass.getProtectionDomain.getCodeSource.getLocation.toURI.getPath

    def codeSourceFile = new File(codeSourcePath)

    def classLoader = this.getClass.getClassLoader

    def isJar = codeSourcePath.endsWith(".jar")

    def getParentFolder = codeSourceFile.getParentFile

    def internalClasses = loadInternalClasses()

    def loadClass(name: String, classLoader: ClassLoader = this.classLoader): Option[Class[_]] = {
        var finalName = name
        if(name.endsWith(".class")) finalName = finalName.replace(".class", "")
        if(name.startsWith(".")) finalName = finalName.substring(1)
        finalName = finalName.replace("/", ".")
        try {
            Option(classLoader.loadClass(finalName))
        } catch {
            case e: ClassNotFoundException =>
                logger.log(Level.WARNING, s"Failed to load class $finalName", e)
                None
        }
    }

    def loadInternalClasses() = {
        if(isJar) {
            loadJarClasses(codeSourceFile)
        } else {
            val loadedClasses = LoadedClasses(CustomClassLoader("internal", classLoader))
            loadedClasses.addClasses(parseDirectoryClasses(codeSourceFile))
            loadedClasses
        }
    }

    def loadJarClasses(file: File): LoadedClasses = {
        val jarFile = new JarFile(file)
        val jarClassLoader = CustomClassLoader(jarFile.getName, classLoader)
        val jarLoadedClasses = LoadedClasses(jarClassLoader)
        jarFile.entries().asScala
            .filter(e => !e.isDirectory && e.getName.toLowerCase().trim.endsWith(".class"))
            .foreach(e => jarLoadedClasses.addClass(loadClass(e.getName, jarClassLoader)))
        jarFile.close()
        jarLoadedClasses
    }

    def parseDirectory(dir: File, jarOnly: Boolean = false, classOnly: Boolean = false): List[LoadedClasses] = {
        if(dir == null || !dir.isDirectory) {
            return List()
        }
        if(jarOnly) {
            parseDirectoryJars(dir)
        } else {
            val loadedClasses = LoadedClasses(CustomClassLoader(dir.getName, classLoader))
            var list = List(loadedClasses)
            if(classOnly) {
                loadedClasses.addClasses(parseDirectoryClasses(dir))
            } else {
                walk(dir).foreach(f => {
                    if(f.getName.endsWith(".jar")) {
                        list ::= loadJarClasses(f)
                    } else if(f.getName.endsWith(".class")) {
                        val c = loadClass(fileToClass(f, dir.getPath))
                        if(c.isDefined) {
                            loadedClasses.addClass(c)
                        }
                    }
                })
            }
            list
        }
    }

    private def parseDirectoryJars(dir: File): List[LoadedClasses] = {
        var list = List[LoadedClasses]()
        walk(dir).foreach(f => {
            if(f.getName.endsWith(".jar")) list ::= loadJarClasses(f)
        })
        list
    }

    private def parseDirectoryClasses(dir: File): List[Class[_]] = {
        if(dir != null && dir.isDirectory) {
            walk(dir, f => loadClass(fileToClass(f, dir.getPath))).flatten
        } else List()
    }

    private def walk(dir: File): List[File] = {
        dir.listFiles flatMap {
            f => if(f.isDirectory) walk(f) else List(f)
        } toList
    }

    private def walk[T](dir: File, closure: File => T): List[T] = {
        walk(dir) map closure
    }

    private def fileToClass(file: File, prefix: String) = {
        file.getAbsolutePath
            .substring(prefix.length)
            .replace(File.pathSeparator, ".")
    }
}
