package io.not2excel.devoid.lib.util.classloading

import java.io.File
import java.net.URLClassLoader

case class CustomClassLoader(name: String, parent: ClassLoader) extends ClassLoader(parent) {
    override def toString = s"`$name` ClassLoader"
}

object CustomClassLoader {
    def apply(file: File, parentClassLoader: ClassLoader): CustomClassLoader = {
        CustomClassLoader(file.getName, new URLClassLoader(Array(file.toURI.toURL), parentClassLoader))
    }
}
