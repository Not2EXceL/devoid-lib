package io.not2excel.devoid.lib.util.implicits

import scala.reflect.ClassTag

object GenericImplicits {

    implicit class AsOpt[A](val a: A) {
        def asOpt[B <: A : ClassTag]: Option[B] = {
            if(classOf[Option[_]].isAssignableFrom(a.getClass))
                a match {
                    case Some(b: B) => Option(b)
                    case _ => None
                }
            else
                a match {
                    case b: B => Option(b)
                    case _ => None
                }
        }
    }

}
